#! /bin/bash
ansible-galaxy install -r requirements.yaml -p roles
ansible-galaxy collection install -r requirements.yaml -p collections
ansible-playbook -i inventory/vagrant.yaml playbook.yaml --list-host
ansible-playbook -i inventory/vagrant.yaml playbook.yaml --list-tasks
read -p "Run playbook in check mode (y/n)?" choice
case "$choice" in
  y|Y ) ansible-playbook -i inventory/vagrant.yaml playbook.yaml --check;;
  n|N ) echo "exiting";;
  * ) echo "invalid";;
esac